### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ bc927840-addf-11eb-0754-c5acaf8ad6f9
using Pkg

# ╔═╡ 65506820-ade0-11eb-1cdf-6f4ca0fd6a96
using PlutoUI

# ╔═╡ 052bf612-b31e-11eb-1d9e-474079c6d848
using DataStructures

# ╔═╡ 593cf9e0-ade0-11eb-2464-114a0d89c531
Pkg.activate("Project.toml")

# ╔═╡ 67ae7c5e-ade0-11eb-1937-b5d9924da2a3
struct Action
Name::String
Cost::Int64
end

# ╔═╡ 477ccc20-ade1-11eb-1de0-8705ceb0289e
East = Action("move east", 3)

# ╔═╡ 55d06662-ade1-11eb-3469-cfadc98f5c90
West = Action("move west", 3)

# ╔═╡ 60c14da0-ade1-11eb-09e4-cff01a349e84
Collect = Action("collect item", 5)

# ╔═╡ 685dd9c0-ade1-11eb-0e90-cbe2d0b9ba89
Remain = Action("remain in office", 1)

# ╔═╡ 7291d9a0-ade1-11eb-24e1-b598530c08c9
struct State
	Name::String
	hasItems::Vector{Bool}
	Position::Int64
end

# ╔═╡ bd8e1280-b312-11eb-336c-9733af96ff81
Zero = State("State Zero", [true, true, true, true], 2) 

# ╔═╡ 5269e560-b31c-11eb-08ee-f5c552e55546
One = State("State One", [true, true, true, true], 2) 

# ╔═╡ 5251062e-b31c-11eb-30fb-81528db05dd8
Two = State("State Two", [true, true, true, true], 2) 

# ╔═╡ 52203230-b31c-11eb-2caf-536829660ca0
Three = State("State Three", [true, true, true, true], 1) 

# ╔═╡ 51fa81c0-b31c-11eb-3df5-f91001101449
Four = State("State Four", [false, true, true, true], 1) 

# ╔═╡ 51d6a610-b31c-11eb-0ee6-67335983c854
Five = State("State Five", [false, true, true, true], 2) 

# ╔═╡ 51bcdc80-b31c-11eb-3bf4-91bda0f9936b
Six = State("State Six", [false, false, true, true], 2) 

# ╔═╡ 51a053ce-b31c-11eb-0540-c1bd2912af6c
Seven = State("State Seven", [false, false, true, true], 3) 

# ╔═╡ 516bd650-b31c-11eb-2784-677b8ddf734b
Eight = State("State Eight", [false, false, true, true], 3) 

# ╔═╡ 51366e6e-b31c-11eb-1c86-2bdbd1e1d361
Nine = State("State Nine", [false, false, true, true], 4) 

# ╔═╡ 511b9370-b31c-11eb-2dc3-6dfd6e29e6cf
Ten = State("State Ten", [false, false, true, false], 4) 

# ╔═╡ 50fb1320-b31c-11eb-10a7-d3fa68b2370b
Eleven = State("State Eleven", [false, false, true, false], 3) 

# ╔═╡ 50ddee30-b31c-11eb-316d-9dd4837b28cb
Twelve = State("State Twelve", [false, false, false, false], 3) 

# ╔═╡ 50734580-b31c-11eb-3475-4f1e756bdd41
TransitionModel = Dict(Zero => [(Collect, One), (Remain, Zero)])

# ╔═╡ 5072a93e-b31c-11eb-0619-137155bcc0d5
push!(TransitionModel, One => [(Collect, Two), (Remain, One)])

# ╔═╡ 506d03f0-b31c-11eb-29da-8f260983f26a
push!(TransitionModel, Two => [(West, Three), (Remain, Two)])

# ╔═╡ b535eba0-b31f-11eb-0806-258dc370b977
push!(TransitionModel, Three => [(Collect, Four), (Remain, Three)])

# ╔═╡ b4d18480-b31f-11eb-2ba5-513dc76ea7c4
push!(TransitionModel, Four => [(East, Five), (Remain, Four)])

# ╔═╡ b49565e0-b31f-11eb-183a-71840ad1aaac
push!(TransitionModel, Five => [(Collect, Six), (Remain, Five)])

# ╔═╡ b479525e-b31f-11eb-0e1d-51f850f1a71e
push!(TransitionModel, Six => [(East, Seven), (Remain, Six)])

# ╔═╡ b45bb83e-b31f-11eb-1471-abe87a77dfc2
push!(TransitionModel, Seven => [(Collect, Eight), (Remain, Seven)])

# ╔═╡ b43ff2e0-b31f-11eb-06e8-996c7e56b0c4
push!(TransitionModel, Eight => [(East, Nine), (Remain, Eight)])

# ╔═╡ b3f184c0-b31f-11eb-3be5-371111109252
push!(TransitionModel, Nine => [(Collect, Ten), (Remain, Nine)])

# ╔═╡ b3d6d0d0-b31f-11eb-1261-ffba868e0add
push!(TransitionModel, Ten => [(West, Eleven), (Remain, Ten)])

# ╔═╡ 506785b0-b31c-11eb-3b5a-910df131f401
push!(TransitionModel, Eleven => [(Collect, Twelve), (Remain, Eleven)])

# ╔═╡ 505f6f60-b31c-11eb-0008-756c192d70f1
goal_state = [Twelve]

# ╔═╡ 8ec55030-b330-11eb-2066-c3266e4e4e5d
initial_state = [Zero]

# ╔═╡ 5047a1a0-b31c-11eb-16e8-7b922b629d49
TransitionModel[Zero]

# ╔═╡ 35477650-b330-11eb-1e93-83d07e83ad6a
function create_result(TransitionModel, ancestors, initial_state, goal_state)
	result = []
	
	while !(goal_state == initial_state)
		current_state_ancestor = ancestors[goal_state]
		related_transitions = TransitionModel[current_state_ancestor]
		for transition in related_transitions
			if transition[2] == goal_state
				push!(result, transition[1])
				break
			else
				continue
			end
		end
		goal_state = current_state_ancestor
	end
	return result
end

# ╔═╡ 4e96e462-b31c-11eb-032e-435302ef6e3c
function path(TransitionModel, initial_state, goal_state, estimated_cost, exact_cost, lowest_cost)
	estimated_cost = 0
	exact_cost = 0
	lowest_cost = 0
	
	result = []
	visited = []
	frontier = Queue{State}()
	ancestors = Dict{State, State}()
	
	
	enqueue!(frontier, initial_state)
	
	while true
		if isempty!(frontier)
		   return []
		else
			current_state = dequeue!(frontier)
			push!(visited, current_state)
			next_states = TransitionModel[current_state]
			for element in next_states
				
				ancestors.exact_cost = current_state.exact_cost + State.cost
				
				#calculating heuristic cost (estimated cost) using Eucladian distance
	            ancestors.estimated_cost = (((ancestors.frontier[0] - goalstate.frontier[0])^2) + ((ancestors.frontier[0] - goalstate.frontier[0])^2))
				
	ancestors.lowest_cost = ancestors.exact_cost + ancestors.estimated_cost
				
				if !(element[2] in visited)
				    push!(ancestors, element[2] => current_state)
					if (element[2] in goal_state)
						return create_result(TransitionModel, ancestors, initial_state, element[2])
					else
						enqueue!(frontier, element[2])
					end
				end
			end
		end
	end
end 

# ╔═╡ Cell order:
# ╠═bc927840-addf-11eb-0754-c5acaf8ad6f9
# ╠═593cf9e0-ade0-11eb-2464-114a0d89c531
# ╠═65506820-ade0-11eb-1cdf-6f4ca0fd6a96
# ╠═052bf612-b31e-11eb-1d9e-474079c6d848
# ╠═67ae7c5e-ade0-11eb-1937-b5d9924da2a3
# ╠═477ccc20-ade1-11eb-1de0-8705ceb0289e
# ╠═55d06662-ade1-11eb-3469-cfadc98f5c90
# ╠═60c14da0-ade1-11eb-09e4-cff01a349e84
# ╠═685dd9c0-ade1-11eb-0e90-cbe2d0b9ba89
# ╠═7291d9a0-ade1-11eb-24e1-b598530c08c9
# ╠═bd8e1280-b312-11eb-336c-9733af96ff81
# ╠═5269e560-b31c-11eb-08ee-f5c552e55546
# ╠═5251062e-b31c-11eb-30fb-81528db05dd8
# ╠═52203230-b31c-11eb-2caf-536829660ca0
# ╠═51fa81c0-b31c-11eb-3df5-f91001101449
# ╠═51d6a610-b31c-11eb-0ee6-67335983c854
# ╠═51bcdc80-b31c-11eb-3bf4-91bda0f9936b
# ╠═51a053ce-b31c-11eb-0540-c1bd2912af6c
# ╠═516bd650-b31c-11eb-2784-677b8ddf734b
# ╠═51366e6e-b31c-11eb-1c86-2bdbd1e1d361
# ╠═511b9370-b31c-11eb-2dc3-6dfd6e29e6cf
# ╠═50fb1320-b31c-11eb-10a7-d3fa68b2370b
# ╠═50ddee30-b31c-11eb-316d-9dd4837b28cb
# ╠═50734580-b31c-11eb-3475-4f1e756bdd41
# ╠═5072a93e-b31c-11eb-0619-137155bcc0d5
# ╠═506d03f0-b31c-11eb-29da-8f260983f26a
# ╠═b535eba0-b31f-11eb-0806-258dc370b977
# ╠═b4d18480-b31f-11eb-2ba5-513dc76ea7c4
# ╠═b49565e0-b31f-11eb-183a-71840ad1aaac
# ╠═b479525e-b31f-11eb-0e1d-51f850f1a71e
# ╠═b45bb83e-b31f-11eb-1471-abe87a77dfc2
# ╠═b43ff2e0-b31f-11eb-06e8-996c7e56b0c4
# ╠═b3f184c0-b31f-11eb-3be5-371111109252
# ╠═b3d6d0d0-b31f-11eb-1261-ffba868e0add
# ╠═506785b0-b31c-11eb-3b5a-910df131f401
# ╠═505f6f60-b31c-11eb-0008-756c192d70f1
# ╠═8ec55030-b330-11eb-2066-c3266e4e4e5d
# ╠═5047a1a0-b31c-11eb-16e8-7b922b629d49
# ╠═35477650-b330-11eb-1e93-83d07e83ad6a
# ╠═4e96e462-b31c-11eb-032e-435302ef6e3c
