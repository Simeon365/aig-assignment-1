### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 332f6450-b3f7-11eb-1f4e-f19f49a3b4b5
using Pkg

# ╔═╡ 8b06ba20-b3f7-11eb-25c8-bb20df004664
Pkg.activate("Project.toml")

# ╔═╡ aa4ed840-b3f7-11eb-17c5-b38f817d631b
@enum NumberDomain one two three four 

# ╔═╡ e48b98de-b3f7-11eb-03d0-bd5e4ee90ff7
mutable struct CSPVariable
    name::String
    value::Union{Nothing,NumberDomain}
    forbidden_values::Vector{NumberDomain}
    domain_restriction_count::Int64
end

# ╔═╡ 1c310cd2-b3f8-11eb-2b65-57d28010f6ec
struct NumberCSP
    variables::Vector{CSPVariable}
    constraints::Vector{Tuple{CSPVariable,CSPVariable}}
end

# ╔═╡ 48e26530-b3f8-11eb-2d36-37faa63bfc55
num = rand(setdiff(Set([one,two,three,four]), Set([four,one])))

# ╔═╡ 940c2fa0-b3f8-11eb-10d9-a79c2b654c48
function solve_csp(pb::NumberCSP, all_assignments)
    for current_variable in pb.variables
        if current_variable.domain_restriction_count == 4
            return []
        else
            next_value = rand(setdiff(Set([one,two,three,four]), Set(current_variable.forbidden_values)))
            current_variable.value = next_value
            for current_constraint in pb.constraints
                if !((current_constraint[1] == current_variable) || (current_constraint[2] == current_variable))
                     continue
                else
                    if current_constraint[1] == current_variable
                       push!(current_constraint[2].forbidden_values, next_value)
                       current_constraint[2].domain_restriction_count += 1
                    else
                       push!(current_constraint[1].forbidden_values, next_value)
                       current_constraint[1].domain_restriction_count += 1
                    end
                end
            end
            push!(all_assignments, current_variable.name => next_value)
        end
     end
     return all_assignments
end

# ╔═╡ a7dca2f0-b3fb-11eb-2b10-b37e977e4822
x1 = CSPVariable("X1",nothing, [], 0)

# ╔═╡ d3210b90-b400-11eb-3ef7-79e9b7b71786
x2 = CSPVariable("X2",nothing, [], 0)

# ╔═╡ d2aaf12e-b400-11eb-00c4-13f41fc4c687
x3 = CSPVariable("X3",one, [], 0)

# ╔═╡ d24e7950-b400-11eb-0a90-13def37dbbf6
x4 = CSPVariable("X4",nothing, [], 0)

# ╔═╡ d1e186b0-b400-11eb-374e-834bc4cc0533
x5 = CSPVariable("X5",nothing, [], 0)

# ╔═╡ d16b1e30-b400-11eb-2bfa-2fc0fa576648
x6 = CSPVariable("X6",nothing, [], 0)

# ╔═╡ d0be3c60-b400-11eb-2e61-cb37d5d6f3b9
x7 = CSPVariable("X7",nothing, [], 0)

# ╔═╡ 088ab290-b3fc-11eb-2a92-0daec98b4840
problem = NumberCSP([x1, x2, x3, x4, x5, x6, x7], [(x1,x2), (x1,x3), (x1,x4),
(x1,x5), (x1,x6), (x2,x5), (x3,x4), (x4,x5), (x4,x6), (x5,x6), (x6,x7)])

# ╔═╡ 18ce5d00-b3fc-11eb-22e3-bb4222f6f200
solve_csp(problem, [])

# ╔═╡ Cell order:
# ╠═332f6450-b3f7-11eb-1f4e-f19f49a3b4b5
# ╠═8b06ba20-b3f7-11eb-25c8-bb20df004664
# ╠═aa4ed840-b3f7-11eb-17c5-b38f817d631b
# ╠═e48b98de-b3f7-11eb-03d0-bd5e4ee90ff7
# ╠═1c310cd2-b3f8-11eb-2b65-57d28010f6ec
# ╠═48e26530-b3f8-11eb-2d36-37faa63bfc55
# ╠═940c2fa0-b3f8-11eb-10d9-a79c2b654c48
# ╠═a7dca2f0-b3fb-11eb-2b10-b37e977e4822
# ╠═d3210b90-b400-11eb-3ef7-79e9b7b71786
# ╠═d2aaf12e-b400-11eb-00c4-13f41fc4c687
# ╠═d24e7950-b400-11eb-0a90-13def37dbbf6
# ╠═d1e186b0-b400-11eb-374e-834bc4cc0533
# ╠═d16b1e30-b400-11eb-2bfa-2fc0fa576648
# ╠═d0be3c60-b400-11eb-2e61-cb37d5d6f3b9
# ╠═088ab290-b3fc-11eb-2a92-0daec98b4840
# ╠═18ce5d00-b3fc-11eb-22e3-bb4222f6f200
