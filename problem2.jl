### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ ee333540-b40d-11eb-3f72-913a7389661d
using Pkg

# ╔═╡ 32736fe2-b40e-11eb-38cb-09ee837130d5
Pkg.activate("Project.toml")

# ╔═╡ 3df3ecf0-b40e-11eb-3b9a-93dd0fd12d2d
Max, Min = 1000, -1000 

# ╔═╡ 4ba92450-b40e-11eb-32b7-ff5639323280
function minimax(depth, nodeIndex, maximizingPlayer, 
            values, alpha, beta)
	if depth == 3 
        return values[nodeIndex]
	end
	if maximizingPlayer  
       
        best = Min 
  
        
        for i in 1:2 
              
            val = minimax(depth + 1, nodeIndex * 2 + i, 
                          false, values, alpha, beta) 
            best = max(best, val) 
            alpha = max(alpha, best) 
  
            
            if beta <= alpha 
                break 
			end
		end
           
        return best 
	else
        best = Max 
  
        for i in 1:2
           
            val = minimax(depth + 1, nodeIndex * 2 + i, 
                            true, values, alpha, beta) 
            best = min(best, val) 
            beta = min(beta, best) 
  
            
            if beta <= alpha 
                break 
			end
		end
        return best 
	end
end

# ╔═╡ 866e00b0-b40e-11eb-12ab-59cc86d0005d
values = [3, 5, 10, 2, 8, 19, 2, 7, 3]

# ╔═╡ 1f4ef730-b40f-11eb-1153-95a76a6cb33d
minimax(3, 1, true, values, Min, Max)

# ╔═╡ Cell order:
# ╠═ee333540-b40d-11eb-3f72-913a7389661d
# ╠═32736fe2-b40e-11eb-38cb-09ee837130d5
# ╠═3df3ecf0-b40e-11eb-3b9a-93dd0fd12d2d
# ╠═4ba92450-b40e-11eb-32b7-ff5639323280
# ╠═866e00b0-b40e-11eb-12ab-59cc86d0005d
# ╠═1f4ef730-b40f-11eb-1153-95a76a6cb33d
